from django.contrib import admin
from .models import Producto
from .forms import *

# Register your models here.

class AdminProducto(admin.ModelAdmin):
    list_display = ["id_producto", "nombre", "descripcion", "categoria"]
    list_filter = ["id_producto", "nombre"]
    search_fields = ["nombre", "categoria"]
    form = ProductoValidator

admin.site.register(Producto, AdminProducto)
