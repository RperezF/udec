from django.shortcuts import render
from .forms import RegForm
from .models import Producto
# Create your views here.

def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        descripcion2 = form_data.get("descripcion")
        categoria2 = form_data.get("categoria")
        sub_categoria2 = form_data.get("sub_categoria")

        objeto = Producto.objects.create(nombre=nombre2, descripcion=descripcion2, categoria=categoria2, sub_categoria=sub_categoria2)

    contexto = {
        "el_formulario": form,
    }

    return render(request, "inicio.html", contexto)
