from django import forms
from .models import Producto
class RegForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    descripcion = forms.CharField(max_length=500)
    categoria = forms.CharField(max_length=100)
    sub_categoria = forms.CharField(max_length=100)

    class Meta:
        model = Producto
        fields=["nombre","descripcion","categoria","sub_categoria"]


    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre)<=1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_descripcion(self):
        descripcion = self.cleaned_data.get("descripcion")
        if descripcion:
            if len(descripcion)<2:
                raise forms.ValidationError("La descripcion no puede ser de un caracter")
            return descripcion
        else:
            raise forms.ValidationError("Este campo es requerido")


    def clean_categoria(self):
        categoria = self.cleaned_data.get("categoria")
        if categoria:
            if len(categoria)<2:
                raise forms.ValidationError("La categoria no puede ser de un caracter")
            return categoria
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_sub_categoria(self):
        subcat = self.cleaned_data.get("sub_categoria")
        if subcat:
            if len(subcat)<2:
                raise forms.ValidationError("La sub - categoria no puede ser de un caracter")
            return subcat
        else:
            raise forms.ValidationError("Este campo es requerido")

class ProductoValidator(forms.ModelForm):
    class Meta:
        model = Producto
        fields=["nombre","descripcion","categoria","sub_categoria"]


    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre)<=1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_descripcion(self):
        descripcion = self.cleaned_data.get("descripcion")
        if descripcion:
            if len(descripcion)<2:
                raise forms.ValidationError("La descripcion no puede ser de un caracter")
            return descripcion
        else:
            raise forms.ValidationError("Este campo es requerido")


    def clean_categoria(self):
        categoria = self.cleaned_data.get("categoria")
        if categoria:
            if len(categoria)<2:
                raise forms.ValidationError("La categoria no puede ser de un caracter")
            return categoria
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_sub_categoria(self):
        subcat = self.cleaned_data.get("sub_categoria")
        if subcat:
            if len(subcat)<2:
                raise forms.ValidationError("La sub - categoria no puede ser de un caracter")
            return subcat
        else:
            raise forms.ValidationError("Este campo es requerido")