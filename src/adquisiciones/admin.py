from django.contrib import admin
from .models import Adquisiciones
from .forms import *

class AdminAdquisiciones(admin.ModelAdmin):
    list_display = ["id_adquisiciones","nombre_producto","nombre_proveedor","cantidad","numero_factura","fecha"]
    list_filter = ["id_adquisiciones" ,"nombre_producto","fecha"]
    search_fields = ["nombre_producto"]
    form = AdquisicionValidator

# Register your models here.
admin.site.register(Adquisiciones, AdminAdquisiciones)