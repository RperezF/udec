from django.db import models

# Create your models here.

class BodegaModel(models.Model):
    id_bodega = models.AutoField(primary_key=True,unique=True)
    nombre_bodega = models.CharField(max_length=100)
    stock_producto = models.IntegerField()
    stock_minimo = models.IntegerField()
    ubicacion=models.CharField(max_length=100)
    fecha_ingreso_bodega=models.DateTimeField()

    def __str__(self):
        return self.nombre_bodega