from django.contrib import admin
from .models import InventarioModel
from .forms import InventarioModelForm

class AdminInventario(admin.ModelAdmin):
    list_display = ["id_inventario","nombre","cantidad","pertenencia_p","pertenencia_s"]
    form = InventarioModelForm


admin.site.register(InventarioModel, AdminInventario)