from django.db import models

class InventarioModel(models.Model):
    id_inventario = models.AutoField(primary_key=True,unique=True)
    nombre = models.CharField(max_length=100, blank=True,null=True)
    cantidad = models.IntegerField()
    pertenencia_p = models.CharField(max_length=100, blank=True,null=True)
    pertenencia_s = models.CharField(max_length=100, blank=True,null=True)

    def __str__(self):
        return self.nombre

# Create your models here.
