from django import forms
from .models import InventarioModel


class InventarioModelForm(forms.ModelForm):
    class Meta:
        model = InventarioModel
        fields=["nombre","cantidad","pertenencia_p","pertenencia_s"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre:
            if len(nombre)<=1:
                raise forms.ValidationError("El nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_pertenencia_p(self):
        pertenencia_p = self.cleaned_data.get("pertenencia_p")
        if pertenencia_p:
            if len(pertenencia_p)<2:
                raise forms.ValidationError("el campo no puede ser de un caracter")
            return pertenencia_p
        else:
            raise forms.ValidationError("Este campo es requerido")

    def clean_cantidad(self):
        cantidad = self.cleaned_data.get("cantidad")

        if cantidad < 0:
            raise forms.ValidationError("La cantidad no puede ser negativo")
        elif cantidad == 0:
            raise forms.ValidationError("La cantidad minima no puede ser cero")
        return cantidad


    def clean_pertenencia_s(self):
        pertenencia_s = self.cleaned_data.get("pertenencia_s")
        if pertenencia_s:
            if len(pertenencia_s) < 2:
                raise forms.ValidationError("el campo no puede ser de un caracter")
            return pertenencia_s
        else:
            raise forms.ValidationError("Este campo es requerido")